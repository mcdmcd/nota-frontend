module.exports = {
    "extends": "standard",
    "env": {"browser": true},
    "rules": {
        "eqeqeq": "off",
        "quotes": "off",
        "keyword-spacing": "off",
        "semi": "off",
        "space-before-function-paren": "off",
        "camelcase": "off",
        "spaced-comment": "off",
        "brace-style": "off",
        "no-multiple-empty-lines": "off",
        "comma-dangle": "off",
        "object-curly-spacing": "off",
        "object-property-newline": "off",
        "space-infix-ops": "off",
        "operator-linebreak": "off",
    }
};
