#!/bin/bash
CURRENT_FAVICON="src/img/favicon.ico"
TEST_FAVICON="src/img/favicon-test.ico"
PRODUCTION_FAVICON="src/img/favicon-production.ico"
TEST_FAVICON_URL="https://test.nota.space/favicon.ico?v=test"
PRODUCTION_FAVICON_URL="https://nota.space/favicon.ico?v=production"
# Change the favicon and the link to it to test
sed -i "" "s+$PRODUCTION_FAVICON_URL+$TEST_FAVICON_URL+" src/index.html
ln -f -v $TEST_FAVICON $CURRENT_FAVICON
npm run build-dev && rsync -au4 --delete dist/* pu@nota.space:/var/www/test.nota.space/html/
# Change favicon and link back to to the production state
ln -f -v $PRODUCTION_FAVICON $CURRENT_FAVICON
sed -i "" "s+$TEST_FAVICON_URL+$PRODUCTION_FAVICON_URL+" src/index.html
