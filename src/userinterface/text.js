let _shouldRefreshText = false;
export function shouldRefreshText(value) {
  if(value !== undefined && value !== null) {
    _shouldRefreshText = value;
  }
  return _shouldRefreshText;
}
