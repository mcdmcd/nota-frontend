import * as positions  from '../positions.js';

let _patches = null;
export function getImagePatches(patchdefs, doUpdate) {
  if(_patches !== null && _patches.length === patchdefs.length && !doUpdate) {
    return _patches;
  }
  else {
    _patches = [];
    let imagefrags = positions.getOnScreenFragments();
    imagefrags = imagefrags.filter(function(frag) {
      return frag.type === 'image';
    });
    let iidx = 0;
    patchdefs.forEach(function(patchdef) {
      let img = null;
      while(img === null && iidx < imagefrags.length) {
        if(imagefrags[iidx].image) {
          img = imagefrags[iidx].image;
        }
        iidx++;
      }
      if(img) {
        let w = patchdef.w;
        let h = patchdef.h;
        _patches.push(getImagePatch(img, {w:w, h:h}));
      }
    });
    return _patches;
  }
}
export function getImagePatch(img, control) {
  let w = control.w;
  let h = control.h;
  let x = Math.round(Math.random() * (img.width - w));
  let y = Math.round(Math.random() * (img.height - h));
  return img.get(x, y, w, h);
}
