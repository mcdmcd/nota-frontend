# Introduction
Welcome! You just opened the frontend code of the open source web-software nota.

nota is a digital rehearsal space, a *Montage tool* for all kinds of media. In nota all media and textfiles are called *fragments* and you can position and manipulate them. The positioning of the fragments opens up a huge digital space you can surf through: move left, right, up, down and zoom around. You can further manipulate nota through *user-scripts*: Every fragment has a script editor that can temporarily add code to notas source text.

To see what happens when your computer reads and executes this source code go to this link: https://nota.space. <br>
To read this code yourself go to the section 'Start Reading nota's Code'. We recommend to read this code in a mixed group of artists and coders. We want different people to be able to read nota's code, which is why we put a team of artists, scientists and programmers on the task of commenting the code together. We also organize *Open-Source Reading Groups*, where we read and edit together nota's and other open source codes: https://nota.space/?user=rora&room=open%20source%20reading%20group

To change nota's code contact us (mail@nota.space) and see the section 'Start Developing notas Code'.

nota is produced by the community around nota e.V. Learn more about nota and it's community here: https://nota.space/?user=nota&room=info

There is another part of notas sourcecode, called backend code. That part is not read by your computer but by a server (The server for https://nota.space is in Bavaria(GER)). To look at that code or set up your own server see this link: https://gitlab.com/nota.space/nota-backend


# Start Reading notas Code

## WHY TO
There may be different reasons to read source codes:

Curious Artists may want to have a critical look at their working tools. They may want to pull back the curtain to see and read the structure of their software. Under this link you will find different examples of artists questioning nota and finding ways to demonstrate and manipulate notas sourcecode and code-structure: https://nota.space/?user=b&room=talking%20code

Collectives and Cultural Institutions may want to take an active part in the Open Source Discourse: For us using the sourcode text as a tool for communication, is a practical way of doing so. Here is a concrete example: in the following link to the Source Code we have commented on one single line of code: https://gitlab.com/nota.space/nota-frontend/-/blob/RC2212-knots-new/src/positions.js#L64 <br>
Open Source can be more than an ideal, reading code can become a shared practice of knowledge production and inspiration.

Coders may want to copy parts of our code, which they are welcome to do. We are still discussing what kind of Open-Source licence we want to choose, so for now just contact us and we can figure something out.

With opening notas code we want to spread our version of creative coding commons and invite people with different levels of experience to think and learn about the creative entaglements of code and artistic practices.



## HOW TO START

### Learn about Comments and Code
Within the code you will find a lot of comments, which help you navigate through the code. You will find information about the structure of the code itself and about how to use it in *user scripts* (especially helpful if you want to get into coding yourself). Comments will look something like this:

```
// this is the first line of notas code, it is a comment for you to read
// the machine will not read it

/*
To learn about the difference between how a computer reads code and how
humans read code see this link: https://nota.space/?user=b&room=circle
*/
```

### Zoom into the machine
nota has a zoom dimension that can get you very close to how a machine works; specifically one that uses the Javascript language.
To experience the *limits of Javascript* click this LINK to a nota space: <br>
https://nota.space/?user=b&room=super%20edge%20of%20js <br>
To discuss the finer, weirder and beautiful details of this dimension start
reading these lines of the source-code and follow where they lead: <br>
https://gitlab.com/nota.space/nota-frontend/-/blob/egg/src/positions.js?ref_type=heads#L98


### Start with Languages and Layers
nota, like most webpages, is a messy mix of different programming languages. These languages correspond to different visual layers. At first glance you cannot distinguish them, since programmers put a lot of work in assembling these layers into an organic webpage. In one of nota spaces you can mess around with these different layers: https://nota.space/?user=b&room=backstage2 <br>
Most of notas user-interface, like menus and buttons, are written in HTML. HTML also provides the basic architecture of nota: For example, it sets up a *canvas* upon which all media fragments are drawn for you to surf through. However HTML does not do any of this *on the fly drawing* on the canvas itself – for this you need a language switch: Javascript is responsible for all moving parts of nota. And since most stuff in nota can be moved or changed, over 90% of nota is written in Javascript. <br>
We also use a special library for Javascript that is made for dynamic drawings. It's called p5.js and it offers a good access for learning to code: https://p5js.org

### Get to know Framerate, Time and Movement
Movement in nota works like it does in the movies: It's an illusion produced by the rapid succession of the images framed by the camera operator. Movement on a screen needs the right kind of pictures and the right amount of frames per second. <br>
Here is a de-constrction of this interplay of image and time: https://nota.space/?user=b&room=muy <br>

nota normally tries to draw the canvas 30 times per second. 30 fps (frames per second) is the North American broadcast standard. So 30 times per second nota will draw the background and all the fragments on it (even when nothing is moving). <br>
Here you can see what happens when do not redraw the the background every frame: https://editor.p5js.org/farzadgo/sketches/BblVstIws

### How to start if you know user-scripts
If you already know about *user scripts* in nota, you can read notas code sort of like an experimental cookbook. You can flip through the sourcecode, find notes for picking ingridients and suggestions on how to combine them.
For example, you can scroll up and click on the folder 'src' and then click on the file 'positions.js' and follow the 'Directions for USER SCRIPTS" to find cooking instructions for the 'goToScene' function.


# Start Developing notas Code
The nota frontend code uses webpack to support modules and 'modern' javascript,
while trying to support older versions of browsers.

npm is used to manage external dependencies, which include webpack

## Install dependencies
This should install webpack, and other dependencies such as p5js


```
# make sure your node and npm version match the current code
# you can ask other developers which versions they are using
npm install
```

## Run development server

```
npm run dev
```

# Why does it say 'egg' in the drop-down menu on the left?
The menu let's you choose different 'branches' of notas fronted source code. 'Branches' are a way to organize work on a sourcecode text. We work on different aspects of the code on these branches, so we don't get in each other's way when working. There are usually branches for new program-features of nota and some branches for commenting on the code. This way different versions of the source text are created. We then use one branch to merge all these edited texts into one version we call the 'egg branch'. Merging is a somewhat delicate process most people never get to see. We try to never break the egg. But if it happens it is not so bad, because the public only gets to see and use this version of nota after we 'rolled' it onto the web address https://nota.space.

Traditionally what we call the 'egg branch' has been called the 'master branch'. A more recent norm is to call it the 'main branch'. But we thought that egg fits our working process better. Eggs are funny and close the gap between a dinosaur and a chicken.
